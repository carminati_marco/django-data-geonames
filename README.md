# README #

This app import cities value using geonames


### How do I get set up? ###

Step to to

After installing the requirements.txt (create a VirtualEnv!!)
```
pip install -r requirements.txt
```

1. Create the database using the command

```
python manage.py migrate
```


2. Create a user to access the admin
```
python manage.py createsuperuser
```

3. Call the command to import the cities

```
python manage.py import_cities --path=/tmp
```

NOTE: As "For the purposes of this challenge, we’re only interested in cities that either
have greater than 15,000 population, or are national capitals.",
I created a command that import the only file we need, i.e. http://download.geonames.org/export/dump/cities5000.zip


4. Runserver

```
python manage.py runserver
```
and go to admin page http://127.0.0.1:8000/admin


NOTE: Due the time, Country and SubNationalRegion are simply
class with a code.
In the next steps It should be a good idea to
add more data to objects.

### Who do I talk to? ###

* carminati.marco@gmail.com