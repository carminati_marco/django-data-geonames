import io
import os
import requests
import zipfile
from django.core.management.base import BaseCommand

from geonames.models import City, Country, SubNationalRegion

ZIP_FILE_URL = 'http://download.geonames.org/export/dump/cities5000.zip'
FILE_NAME = 'cities5000.txt'


def _is_float(input):
    """
    function to detect if it's floa a string
    :param input:
    :return:
    """
    try:
        float(input)
    except ValueError:
        return False
    return True


class Command(BaseCommand):
    help = 'This command resets City mondel and imports all cities that either have greater than 15,000 population, ' \
           'or are national capitals.'

    def add_arguments(self, parser):
        parser.add_argument(
            '--path', dest='path', required=True,
            help='the path to save file',
        )

    def handle(self, *args, **options):
        # delete all city, country and subnationalreagion.
        City.objects.all().delete()
        Country.objects.all().delete()
        SubNationalRegion.objects.all().delete()

        path = options['path']
        # download and extract the file.
        r = requests.get(ZIP_FILE_URL)
        z = zipfile.ZipFile(io.BytesIO(r.content))
        z.extractall(path)

        # process the extracted file
        with open(os.path.join(path, FILE_NAME)) as cities:
            for line in cities:
                # split the line using tabs as separator.
                split_line = [splits for splits in line.split("\t") if splits is not ""]

                # get lat + long data
                latitude = split_line[4] if _is_float(split_line[4]) else None
                longitude = split_line[5] if _is_float(split_line[5]) else None

                # get (with eventually creation) the country and sub_national_region
                country, created = Country.objects.get_or_create(code=split_line[8])
                sub_national_region, created = SubNationalRegion.objects.get_or_create(code=split_line[9])

                # create the city.
                City.objects.create(name=split_line[1], latitude=latitude,
                                    longitude=longitude,
                                    country=country,
                                    sub_national_region=sub_national_region)