from django.db import models


class Country(models.Model):
    code = models.CharField(max_length=5)

    class Meta:
        verbose_name_plural = "Countries"

    def __unicode__(self):
        return self.code


class SubNationalRegion(models.Model):
    code = models.CharField(max_length=5)

    def __unicode__(self):
        return self.code

class City(models.Model):
    """
    City
    """
    name = models.CharField(max_length=255)
    latitude = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)
    longitude = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)
    sub_national_region = models.ForeignKey(SubNationalRegion)
    country = models.ForeignKey(Country)

    class Meta:
        verbose_name_plural = "Cities"

    def __unicode__(self):
        return self.name

