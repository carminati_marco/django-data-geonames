

from django.contrib import admin

from geonames.models import City, Country, SubNationalRegion


class CountryAdmin(admin.ModelAdmin):
    pass
admin.site.register(Country, CountryAdmin)

class SubNationalRegionAdmin(admin.ModelAdmin):
    pass
admin.site.register(SubNationalRegion, SubNationalRegionAdmin)

class CityAdmin(admin.ModelAdmin):
    list_display = ('name', 'latitude', 'longitude', 'sub_national_region', 'country')
    search_fields = ('name', 'latitude', 'longitude', 'sub_national_region__code', 'country__code')

admin.site.register(City, CityAdmin)